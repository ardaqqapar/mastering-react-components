import { useEffect } from 'react'
import Header from './Header'
import Main from './Main'
import Photos from './Photos'
import Footer from './Footer'

function Page() {
    return (
        <>
            <Header title='My title'/>
            <Main>
                <Photos />
            </Main>
            <Footer />
        </>
    )
}
export default Page;