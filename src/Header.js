function Header({ title='Header Title' }) {
    return(
        <>
            <h1>{title}</h1>
        </>
    )
}

export default Header