import { photos } from './PhotosList'

function Photos() {
    return(
        <> 
            {photos.map(photo => (
                <div key={photo.id}>
                    <img height={100} src={photo.url}/>
                </div>
            ))}
        </>
    )
}

export default Photos