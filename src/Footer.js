import PropTypes from 'prop-types'

function Footer({ title='Footer Title' }) {
    return(
        <>
            <h1>{title}</h1>
        </>
    )
}

Footer.propTypes = {
    title: PropTypes.string
}

export default Footer